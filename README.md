# Catwalker

Catwalker offers a fast and easy opportunity to share information about a pet of a facility. They are published on a timeline, that other registered users can see. That way you get to know about activities, favorite places and habits of the campus cat.
<br/><br/>



03/2016 – 06/2016 | Android Smartphones



Overview of all Sightings |  Adding a Sighting | Locating the pet
:-------------------------:|:-------------------------:|:----:
<img src="images/catwalker3.png" alt="Overview" width="200px">  |  <img src="images/catwalker1.png" alt="New Sighting" width="200px"> | <img src="images/catwalker2.png" alt="Locating the pet" width="200px">
